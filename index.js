console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:
console.log("\nObjective 1: \n\n")

let firstName = 'Nayeon';
console.log(`My first name is: ${firstName}`);

let lastName = 'Im'
console.log(`My last name is: ${lastName}`);

let myAge = 26;
console.log(`I am: ${myAge} years old.`);

let hobbies = ['eating', 'sleeping', 'drinking'];
console.log("My hobbies include: ")
console.log(hobbies);

let workAddress = {
	houseNumber: 1234,
	street: "Eskinita Japan",
	city: "Cebu City",
	state: "San Francisco"
}
console.log("My work adress: ");
console.log(workAddress);



/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
console.log("\n\n\n Objective 2: \n\n")
	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		userName: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestFriendName = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriendName);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
